const { replace, toLower, toUpper } = require('ramda')

const capitalize = function(string) {
  return replace(/(^|[^a-zA-Z\u00C0-\u017F'])([a-zA-Z\u00C0-\u017F])/g, toUpper)(toLower(string))
}

module.exports = capitalize
