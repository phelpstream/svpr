//@ts-check
const { is, curryN } = require('ramda')
const digBaseP = require('./digBaseP')

const digP = async (fn, object, { key, path } = {}) => Promise.resolve(object).then(objectP => digBaseP(digP, fn, objectP, { key, path }))

module.exports = digP

// let o = {
//   title: 'title',
//   h2: 'h2',
//   arr: [1, 2],
//   detail: {
//     $s: 'ul li.item',
//     $a: 'text cmp'
//   },
//   re: {
//     cur: {
//       sive: 'ok'
//     }
//   }
// }

// let o = [1, "coucou"]

// let o = {
//   title: 'title',
//   h2: 'h2',
//   arr: [1, { ok: 'ok' }]
// }

// ;(async () => {
//   let r = await digP((value, { key, path, at_filter_time, at_edit_time }) => {
//     if (at_filter_time) {
//       console.log('editing', 'key', key, 'value', value, 'path', path)
//       if (is(Object, value)) {
//         if (value.$s) delete value.$s
//         return value
//       }
//       if (is(String, value)) {
//         // console.log('reversing', value)
//         return value
//           .split('')
//           .reverse()
//           .join('')
//       }
//     }

//     return value
//   }, o)

// let r = await digP((value, index, path, filtering) => {
//   if (!path) {
//     console.log('value', value)
//   }
//   console.log('path', path, 'filtering', filtering)
//   if (is(Object, value)) {
//     if (value.$s) delete value.$s
//     return value
//   }
//   if (is(String, value)) {
//     // console.log('reversing', value)
//     return value
//       .split('')
//       .reverse()
//       .join('')
//   }
//   return value
// }, o)

//   console.log(JSON.stringify(r, null, 2))
// })()
