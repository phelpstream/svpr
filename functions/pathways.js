const { curryN, flatten, split, times, isNil, reject } = require('ramda')
const getOr = require('./getOr')

const pathways = curryN(2, (path_definition, obj) => {
  const buildMe = path_def => {
    let parsed_path = split('..')(path_def)
    let safe_path = parsed_path[0]
    let sub_array_path = parsed_path.slice(1).join('..')

    // console.log('parsed_path', parsed_path, 'safe_path', safe_path, 'sub_array_path', sub_array_path)

    if (sub_array_path) {
      let l = getOr([], safe_path, obj).length
      if (l > 0) {
        return times(i => `${safe_path}.${i}.${sub_array_path}`, l).map(item => (item.includes('..') ? buildMe(item) : item))
      } else {
        return
      }
    }

    return safe_path
  }
  return reject(isNil, flatten([buildMe(path_definition)]))
})

module.exports = pathways

// const o = {
//   root: {
//     a: [
//       {
//         b: {
//           c: [
//             {
//               d: [1, 2, 3]
//             }
//           ]
//         }
//       },
//       {
//         b: {
//           c: [
//             {
//               d: [1, 2, 3]
//             },
//             {
//               d: [1, 2, 3]
//             },
//             {
//               d: [1, 2, 3]
//             }
//           ]
//         }
//       }
//     ]
//   }
// };

// // setFn("vals.0.ok", true, o)
// // setFn("vals..ok..", false, o);

// let paths = pathways("root.a..b.c..d", o);
// console.log("paths", paths);
