//@ts-check
const { mapObjIndexed, binary, curryN, ifElse, is, pipe, values } = require("ramda")
const eachIndexed = curryN(2, (fn, value) => ifElse(is(Array), pipe(mapObjIndexed(binary(fn)), values), mapObjIndexed(binary(fn)))(value))
module.exports = eachIndexed
