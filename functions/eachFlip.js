const { flip } = require("ramda")
const each = require("./each")
const eachFlip = flip(each)
module.exports = eachFlip

// eachFlip(
//   {
//     ok: true,
//     no: false
//   },
//   console.log
// )

// each(console.log, {
//   ok: true,
//   no: false
// })
