//@ts-check
const { curryN } = require("ramda")
const captureBase = require("./captureBase")

const captureAs = curryN(2, (keysList, regex, value) => captureBase(false, keysList, regex, value))
module.exports = captureAs

// exemple

// let s = "Some boring text"
// let r = /(\w+)/gim

// let result = captureAs(["word"], r, s)
// console.dir(result)
