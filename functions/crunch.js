const { is } = require('ramda')
const clone = require('./clone')
const IsBuffer = require('is-buffer')

function crunch(target, opts = {}) {
  const { includeTree, keepLists } = opts
  const delimiter = '.'
  let output = {}

  function step(object, prev) {
    Object.keys(object).forEach(function(key) {
      const value = object[key]
      const isBuffer = IsBuffer(value)
      const isObject = is(Object, value)
      const isArray = is(Array, value)

      const newKey = prev ? prev + delimiter + key : key

      if (!isBuffer && isObject && Object.keys(value).length) {
        if (includeTree) output[newKey] = null
        if (keepLists && isArray) {
          output[newKey] = clone(value)
          return output[newKey].forEach((v, i) => crunch(output[newKey][i], opts))
        }
        return step(value, newKey)
      }

      output[newKey] = value
    })
  }

  step(target)

  return output
}

module.exports = crunch

// test

// let o = {
//   title: 'title',
//   h2: 'h2',
//   arr: [1, 2],
//   detail: {
//     $s: 'ul li.item',
//     $a: 'text cmp'
//   },
//   re: {
//     cur: {
//       sive: 'ok'
//     }
//   }
// }
// // console.time('crunch')
// console.log(crunch(o, { keepLists: true }))
// console.timeEnd('crunch')
