const { curryN } = require('ramda')

const next = curryN(2, (index, arr) => {
  if (arr.length - 1 >= index + 1) {
    return arr[index + 1]
  } else {
    return arr[0]
  }
})

module.exports = next

// const a = [1, 2, 3]
// console.log(next(0, a))
// console.log(next(1, a))
// console.log(next(2, a))
// console.log(next(3, a))
