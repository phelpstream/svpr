//@ts-check
const { mapObjIndexed, unary, curryN, values, is, ifElse, pipe } = require("ramda")
const each = curryN(2, (fn, value) => ifElse(is(Array), pipe(mapObjIndexed(unary(fn)), values), mapObjIndexed(unary(fn)))(value))
module.exports = each

// let v = each(v => v + 1, [1, 3])
// let v = each(v => v + 1, {
	// a: 1,
	// c: 3
// })

// console.log(v)
