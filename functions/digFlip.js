const dig = require("./dig")
const { flip } = require("ramda")

const digFlip = flip(dig)
module.exports = digFlip

// const { is } = require("ramda")

// let o = {
//   title: "title",
//   h2: "h2",
//   arr: [1, 2],
//   detail: {
//     $s: "ul li.item",
//     $a: "text cmp"
//   },
//   re: {
//     cur: {
//       sive: "ok"
//     }
//   }
// }

// console.log(
//   JSON.stringify(
//     digFlip(o, (value, index) => {
//       if (is(Object, value)) {
//         return value
//       } else if (is(String, value)) {
//         return value
//           .split("")
//           .reverse()
//           .join("")
//       }
//       return value
//     }),
//     null,
//     2
//   )
// )
