//@ts-check
const { keys, length, is, isNil, last } = require("ramda")
const get = require("./get")
const has = require("./has")

function Stack(initialValue) {
  if (!(this instanceof Stack)) return new Stack(initialValue)
  let stackPile = []

  this.set = function(value, meta = {}) {
    let entry = { data: value, _t: new Date().getTime(), meta, index: stackPile.length }
    stackPile.push(entry)
    return entry
  }

  this.value = function(value, meta = {}) {
    if (value) this.set(value, meta)
    return get("data", last(stackPile))
  }

  this.values = function() {
    return stackPile
  }

  this.isStack = possibleStack => {
    return has("constructor", possibleStack) && possibleStack.constructor.name === this.constructor.name
  }

  if (initialValue) this.value(initialValue)

  return this
}

module.exports = Stack

// let stack = Stack()
// console.dir(stack.value({ ok: true }))
// console.log(stack.values());
// console.log(Stack().isStack(stack));
