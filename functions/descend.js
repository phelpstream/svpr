const { is } = require("ramda")

const descend = (a, b) => {
  try {
    if (is(Object, a) && !is(Array, a)) return JSON.stringify(b).localeCompare(JSON.stringify(a))
  } catch (error) {
    // Might not be safe if circular, etc
  }
  if (is(String, a)) return b.localeCompare(a)
  return b - a
}

module.exports = descend
