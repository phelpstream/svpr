const { curryN, flip, all } = require("ramda");
const has = require("./has");

const hasAll = curryN(2, function hasPath(keyPathList, obj) {
  return all(flip(has)(obj), keyPathList);
});

module.exports = hasAll;

// console.log(hasAll(["something.ok", "something,notok"], { something: { ok: true } }));
