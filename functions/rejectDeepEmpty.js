//@ts-check
const { isEmpty, trim, pipe, when, is } = require("ramda")
const rejectDeep = require("./rejectDeep")
const rejectDeepEmpty = rejectDeep(pipe(when(is(String), trim), isEmpty))

module.exports = rejectDeepEmpty

// let o = {
//   name: "SPODNÍ PRÁDLO  ",
//   id: 100,
//   website: "",
//   contactDetails: {
//     street: "Kpt.Jaroše 59",
//     secondLineStreet: " ",
//     city: "Kadaň",
//     postalCode: "43201",
//     country: "CZ",
//     state: "",
//     latitude: 50.38231,
//     longitude: 13.270841,
//     phone: "+420 731 181 706",
//     email: "paja.knollova@seznam.cz"
//   },
//   category: "Lisca, all"
// }

// console.log(rejectDeepEmpty(o))
