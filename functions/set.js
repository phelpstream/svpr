const { set, split, lensPath, curryN, isNil } = require("ramda");
const pathArray = require("./pathArray");

const setFn = curryN(3, (path, value, obj) => set(lensPath(pathArray(path)), value, !isNil(obj) ? obj : {}));
module.exports = setFn;

// sample
// let o = { ok: "super", arr: ["un", "deux"] };
// let p = "arr.2";
// let pp = split(".")(p).map(i => parseInt(i) || i);

// console.log(pp);
// console.log(setFn(p, "trois", o));

// let o = {};
// o = setFn("ok.sub", "amazing", o);
// o = setFn("ok.sub2", "amazing2", o);
// console.log(o);
