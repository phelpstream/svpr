//@ts-check
const { ifElse, pipe, is, mapObjIndexed, curryN, map, unary } = require("ramda")
const digBase = require("./digBase")

const dig = curryN(2, (fn, object) => digBase(dig, fn, object))

module.exports = dig

// let o = {
//   title: "title",
//   h2: "h2",
//   arr: [1, 2],
//   detail: {
//     $s: "ul li.item",
//     $a: "text cmp"
//   },
//   re: {
//     cur: {
//       sive: "ok"
//     }
//   }
// }

// console.log(
//   JSON.stringify(
//     dig((value, index) => {
//       if (is(Object, value)) {
//         return value
//       } else if (is(String, value)) {
//         return value
//           .split("")
//           .reverse()
//           .join("")
//       }
//       return value
//     }, o),
//     null,
//     2
//   )
// )
