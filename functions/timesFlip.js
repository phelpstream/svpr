const { times, flip } = require("ramda")
module.exports = flip(times)
