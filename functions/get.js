const { split, curryN, isNil, is } = require('ramda')
const path = require('./path')
const pathways = require('./pathways')
const captureAll = require('./captureAll')
const CAPTURE_KEYS = /(?!\{[^.}]+?)(\.)(?![^.{]+?\})/gim

const get = curryN(2, (keyPath, obj) => {
  if (!is(String, keyPath)) return
  let keyPaths = is(Array, keyPath) ? keyPath : []
  if (keyPath.includes('..')) {
    keyPaths = pathways(keyPath, obj)
  }
  if (keyPaths.length > 1) {
    return keyPaths.map(i => get(i, obj))
  } else {
    let keyPaths = []
    let temporaryKeyPath = `${keyPath}`
    let captured = captureAll(CAPTURE_KEYS, temporaryKeyPath)
    while (captured.length > 0) {
      // console.log("captured", captured);
      keyPaths.push(temporaryKeyPath.substring(0, captured[0].index))
      temporaryKeyPath = temporaryKeyPath.substring(captured[0].index + 1)
      captured = captureAll(CAPTURE_KEYS, temporaryKeyPath)
    }
    keyPaths.push(temporaryKeyPath)
    // console.log("keyPaths", keyPaths);
    return path(keyPaths, !isNil(obj) ? obj : {})
  }
})

module.exports = get

// sample
// let getName = get("identity.name.0")
// let getZero = get("identity.0")
// let getGabinObj = get("{person.age=23}.name")
// let arr = ["Gabin"]:
// let profile = { identity: { name: ["Gabin"], "0": "super" } }
// let idedArr = [{ name: "Gabin", age: 23 }, { name: "Robin" }]
// console.log(get("{$last}", idedArr));
// console.log(get("{name=Gabin}.age", idedArr));
// console.log(getName(profile))
// console.log(getZero(profile))
// console.log(get(0, arr))
// console.log(getGabinObj(idedArr))

// let getNames = get('people..identity.name..item')
// let people = { people: [{ identity: { name: [{ item: 'Gabin' }] } }, { identity: { name: [{ item: 'John' }] } }] }
// console.log(getNames(people))
// let getClaim = get("persons.{details.age=23}.claims.{id=123}")
// let keys = captureAll(CAPTURE_KEYS, "persons.{details.age=23}.claims.{id=123}")
// console.log(keys);
// let o = { persons: [{ name: "Gabin", details: { age: 23 }, claims: [{ id: 123, ok: true }] }, { name: "Julie" }] }
// console.log("c", getClaim(o));