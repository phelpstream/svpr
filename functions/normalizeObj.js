const { sortBy, identity } = require("ramda")
const entries = require("./entries")
const get = require("./get")

const normalizeObj = function(obj, normalizeFn = identity) {
  let sortedByKeys = sortBy(get(0), entries(obj))
  return sortedByKeys.reduce((sortedObj, [key, value]) => {
    sortedObj[key] = normalizeFn(value)
    return sortedObj
  }, {})
}

module.exports = normalizeObj

// console.log(
//   normalizeObj({
//     lastName: "Desserprit",
//     firstName: "Gabin",
//     age: 26
//   })
// )
