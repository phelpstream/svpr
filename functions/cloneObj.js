//@ts-check
const cloneObj = obj => {
  try {
    return Object.assign({}, obj)
    // return JSON.parse(JSON.stringify(obj))
  } catch (error) {
    return {}
  }
}

module.exports = cloneObj
