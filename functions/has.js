//@ts-check
const { hasPath, pathSatisfies, split, curryN, not, pipe, isNil, is } = require("ramda");
// const has = curryN(2, (keyPath, obj) => {
//   // try {
//     return hasPath(split(".")(`${keyPath}`), obj);
//   // } catch (error) {
//   //   // console.error(error);
//   //   console.log("error", keyPath, obj);
//   //   return false;
//   // }
// });

const has = curryN(2, function hasPath(keyPath, obj) {
  keyPath = split(".")(`${keyPath}`);
  if (keyPath.length === 0 || isNil(obj)) {
    return false;
  }
  var val = obj;
  var idx = 0;
  while (idx < keyPath.length) {
    if (!isNil(val) && is(Object, val) && val.hasOwnProperty(keyPath[idx])) {
      val = val[keyPath[idx]];
      idx += 1;
    } else {
      return false;
    }
  }
  return true;
});

// const has = curryN(2, (keyPath, obj) => pathSatisfies(pipe(isNil, not), split(".")(`${keyPath}`), obj))
module.exports = has;
// sample

// console.log(has("something.ok", { something: { ok: true } }))
// console.log(has("something.ok", { something: { no: true } }))
