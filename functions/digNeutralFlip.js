const digSafe = require("./digSafe")
const { flip } = require("ramda")

module.exports = flip(digSafe)
