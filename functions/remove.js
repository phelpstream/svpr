const { init, last, curryN, is } = require('ramda')
const get = require('./get')
const clone = require('./clone')

const remove = curryN(2, (path, obj) => {
  let pathArr = path.split(".")
  let initPath = init(pathArr)
  let lastPath = last(pathArr)
  let rootObj = clone(obj)
  let rootObjPointer = rootObj
  if (initPath.length > 0) {
    rootObjPointer = get(initPath.join('.'), rootObjPointer)
  }
  if (rootObjPointer.hasOwnProperty(lastPath)) {
    if (is(Array, rootObjPointer)) {
      rootObjPointer.splice(+lastPath, 1)
    } else {
      delete rootObjPointer[lastPath]
    }
  }
  return rootObj
})

module.exports = remove

// sample
// let rem = remove("identity.arr.1")
// let profile = { identity: { name: "Gabin", arr: [1, 2, 3] } }

// console.log(profile, rem(profile));
