const { __, curryN } = require('ramda')
// const stringify = curryN(3, JSON.stringify)(__, null, 2)
const fastStringify = require('fast-stringify')
const stringify = (obj, indent) => fastStringify(obj, undefined, indent)
module.exports = stringify

// console.log(stringify({
// 	ok: "true"
// }));
