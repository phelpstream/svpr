const { curryN } = require('ramda')
const clone = require('./clone')
const pick = require('./pick')

const pickList = curryN(2, (paths, referenceObj) => {
  return paths.reduce((o, path) => pick(path, clone(referenceObj), o), {})
})

module.exports = pickList

// const o = {
//   root: {
//     a: [
//       {
//         b: {
//           c: [
//             {
//               d: [1, 2, 3]
//             }
//           ]
//         }
//       },
//       {
//         b: {
//           c: [
//             {
//               d: [1, 2, 3]
//             },
//             {
//               d: [1, 2, 3]
//             },
//             {
//               d: [1, 2, 3]
//             }
//           ]
//         }
//       }
//     ],
//     a2: true,
//     a3: false
//   },
//   root2: {
//     a: true
//   }
// }

// let res = pickList(['root.a2', 'root.a3', 'root2.a'], o)
// console.log(JSON.stringify(res, null, 2))

// let c = {
//   booking: {
//     ok: 'super'
//   }
// }

// let res = pickList(['booking'], c)
// console.log(JSON.stringify(res, null, 2))
