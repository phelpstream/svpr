const { path, split, curryN, isNil } = require('ramda')

const getOr = curryN(3, (defaultValue, keyPath, obj) => {
  let value = path(split('.')(`${keyPath}`), !isNil(obj) ? obj : {})
  return value ? value : defaultValue
})

// const getOr = curryN(3, (defaultValue, keyPath, obj) => pathOr(defaultValue, split(".")(`${keyPath}`), obj))
module.exports = getOr

// sample
// let getName = getOr("Unknown", "identity.name")
// let profile = { identity: { name: "Gabin" } }
// console.log(getName(profile))

// console.log(getOr("Unknown", 0, []))
