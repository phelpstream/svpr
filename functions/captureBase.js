//@ts-check
const { is } = require("ramda")
const get = require("./get")

function captureBase(multiple = true, keys = null, regex, value) {
  let results = []
  let flags = regex.flags || ""
  let localRegex = new RegExp(regex.source, regex.flags)
  if (!flags.includes("g")) localRegex = new RegExp(regex.source, `${flags}g`)

  if (is(String, value)) {
    let resultTable
    let continueToLoop = true
    while (continueToLoop && (resultTable = localRegex.exec(value)) !== null) {
      if (resultTable) {
        if (is(Array, keys)) {
          results.push(
            keys.reduce((obj, key, keyIndex) => {
              obj[key] = resultTable.slice(1)[keyIndex] || null
              return obj
            }, {})
          )
        } else {
          results.push({
            input: resultTable.input,
            matched: resultTable[0],
            index: resultTable.index,
            groups: resultTable.slice(1)
          })
        }
      }
      if (!multiple) continueToLoop = false
    }
  }

  return multiple ? results : results[0] || null
}

module.exports = captureBase

// exemple

// let s = "Some boring text"
// let r = /(\w+)/gim

// let result = captureBase(true, ["word"], r, s)
// console.dir(result)