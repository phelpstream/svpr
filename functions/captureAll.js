//@ts-check
const { curryN } = require("ramda")
const captureBase = require("./captureBase")

const captureAll = curryN(2, (regex, value) => captureBase(true, null, regex, value))
module.exports = captureAll

// exemple

// let s = "Some boring text"
// let r = /(\w+)/gim

// let result = captureAll(r, s)
// console.dir(result)
