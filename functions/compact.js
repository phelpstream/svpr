const { replace } = require("ramda")
const compact = replace(/\s+/gm, " ")
module.exports = compact

//test

// let v = compact("  Un text   avec beaucoup       d'espaces   irréguliers")
// console.log(v);
