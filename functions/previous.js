const { curryN } = require('ramda')

const previous = curryN(2, (index, arr) => {
  if (index - 1 >= 0) {
    return arr[index - 1]
  } else {
    return arr[arr.length - 1]
  }
})

module.exports = previous

// const a = [1, 2, 3]
// console.log(previous(0, a))
// console.log(previous(1, a))
// console.log(previous(2, a))
// console.log(previous(3, a))