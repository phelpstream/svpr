const { flip } = require("ramda")
const digStop = require("./digStop")

const digStopFlip = flip(digStop)
module.exports = digStopFlip
