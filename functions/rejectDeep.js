//@ts-check
const { pipe, reject, map, curryN, when, either, is } = require("ramda")
const rejectDeep = curryN(2, (predicator, filterable) =>
  when(either(is(Array), is(Object)), pipe(reject(predicator), map(rejectDeep(predicator))))(filterable)
)
module.exports = rejectDeep

// sample

// let arr = [1, null, 4, 4]
// let obj = {
//   ok: true,
//   nullable: null,
//   sub: {
//     ok: "shit",
//     no: null
//   }
// }

// let rejectDeepNulls = rejectDeep(isNil)

// let obj = {
//   ok: true,
//   nullable: null,
//   sub: {
//     ok: "shit",
//     no: []
//   }
// }

// let rejectDeepNulls = rejectDeep(isNil)


// console.log(rejectDeepNulls(arr))
// console.log(rejectDeepNulls(obj))
// console.log(rejectDeep(obj))
