const { is, identity } = require("ramda")
const normalizeArr = require("./normalizeArr")
const normalizeObj = require("./normalizeObj")

const normalize = function(value, normalizeFn = identity) {
  if (is(Array, value)) return normalizeArr(value, normalize)
  else if (is(Object, value)) return normalizeObj(value, normalize)
  else return normalizeFn(value)
}

module.exports = normalize

// console.log(normalize([{ d: "ok" }, { b: "ok", a: "ok" }, { c: "ok" }]))
// console.log(normalize([1, 5, 3, 0, 29, 12, 4]))

// console.log(
//   normalize({
//     lastName: "Desserprit",
//     firstName: "Gabin",
//     age: 26
//   })
// )
