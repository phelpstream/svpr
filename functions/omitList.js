const { curryN } = require('ramda')
const clone = require('./clone')
const omit = require('./omit')

const omitList = curryN(2, (paths, referenceObj) => {
  // console.time('omitList')
  let omitted = paths
    .sort((a, b) => {
      // sort from root to deep
      let aLength = a.split('.').length - 1
      let bLength = b.split('.').length - 1
      return aLength - bLength
    })
    .reduce((o, path) => omit(path, o), clone(referenceObj))
  // console.timeEnd('omitList')
  return omitted
})

module.exports = omitList

// let o = { a: 'top', b: [{ a: 'ok' }, { b: 'ok' }] }
// let o = { a: 'top', b: [] }
// let result = omitList(['b..a'], o)
// console.log('result', result)
