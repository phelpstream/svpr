//@ts-check
const { curryN } = require("ramda")
const captureBase = require("./captureBase")

const captureAllAs = curryN(3, (keysList, regex, value) => captureBase(true, keysList, regex, value))
module.exports = captureAllAs

// exemple

// let s = "Some boring text"
// let r = /(\w+)/gim

// let result = captureAllAs(["word"], r, s)
// console.dir(result)
