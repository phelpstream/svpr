const { curryN } = require('ramda')
const { deepEqual } = require('fast-equals')
// const equal = require('fast-deep-equal')
const equals = curryN(2, deepEqual)
// const equals = curryN(2, equal)
module.exports = equals

// test

// let obj1 = {
//   one: 1,
//   two: 2,
//   three: 3
// }

// let obj2 = {
//   one: 1,
//   two: 2,
//   three: 4
// }

// let v = equals(obj1, obj2)
// console.log(v)
