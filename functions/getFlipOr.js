const { __, curryN } = require("ramda")
const getOr = require("./getOr")

const getFlipOr = curryN(3, (defaultValue, obj, keyPath) => getOr(defaultValue, keyPath, obj))
module.exports = getFlipOr

// example

// let o = {
// 	a: 1,
// 	b: 2
// }

// let getFromO = getFlipOr("empty", o)

// console.log(getFromO("a"));
// console.log(getFromO("b"));
// console.log(getFromO("c"));