const { flip } = require("ramda")
const get = require("./get")

const getFlip = flip(get)
module.exports = getFlip


// example

// let o = {
// 	a: 1,
// 	b: 2
// }

// let getFromO = getFlip(o)

// console.log(getFromO("a"));
// console.log(getFromO("b"));
// console.log(getFromO("c"));