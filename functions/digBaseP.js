//@ts-check
const { ifElse, pipeP, pipe, is, mapObjIndexed, curryN, map, unary } = require('ramda')
const clone = require('./clone')

const digBaseP = async (digger, fn, objectP, { key, path } = {}) => {
  const makePath = k => (path ? path + `.${k}` : k)

  const promFn = (value, index, updatePath = true, at_filter_time = true) =>
    Promise.resolve(fn(value, { key: index, path: updatePath ? makePath(index) : path, at_filter_time, at_edit_time: !at_filter_time }))

  if (is(Array, objectP)) {
    return Promise.all(objectP.map((value, index) => digger(fn, promFn(value, index), { key: index, path: makePath(index) })))
  } else if (is(Object, objectP)) {
    return Object.entries(objectP).reduce(async (reducedP, [k, value]) => {
      return reducedP.then(async reduced => {
        reduced[k] = await digger(fn, promFn(value, k), { key: k, path: makePath(k) })
        return reduced
      })
    }, Promise.resolve({}))
  } else {
    return promFn(objectP, key, false, false)
  }
}

module.exports = digBaseP
