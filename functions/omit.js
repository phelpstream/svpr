const R = require('ramda')
const set = require('./set')
const get = require('./get')
const has = require('./has')
const clone = require('./clone')
const pathways = require('./pathways')
const pathArray = require('./pathArray')

const omit = R.curryN(2, (path, referenceObj) => {
  // console.time('omit:pathways')
  let pw = pathways(path, referenceObj)
  // console.timeEnd('omit:pathways')
  // console.log("pw", pw);
  // console.time('omit:reduce')
  let reduced = pw.reduce((o, p) => {
    let pathExists = has(p, o)
    if (pathExists) {
      // console.time('omit:pathArray')
      let path_arr = pathArray(p)
      // console.timeEnd('omit:pathArray')
      // console.log("path_arr", path_arr);
      let sub_path = path_arr.slice(0, -1).join('.')
      // console.log("sub_path", sub_path);
      let key = R.last(path_arr)
      // console.log("key", key);
      let result
      if (sub_path) {
        // console.time('omit:sub_path_result')
        result = set(sub_path, R.omit([key], get(sub_path, o)), o)
        // console.timeEnd('omit:sub_path_result')
      } else {
        // console.time('omit:not_sub_path_result')
        result = R.omit([key], o)
        // console.timeEnd('omit:not_sub_path_result')
      }
      return result
    } else {
      // console.log("p", p, pathExists, o);
    }

    return o
  }, clone(referenceObj))

  // console.timeEnd('omit:reduce')
  return reduced
})

module.exports = omit

// const o = {
//   root: {
//     a: [
//       {
//         b: {
//           c: [
//             {
//               d: [1, 2, 3]
//             }
//           ]
//         }
//       },
//       {
//         b: {
//           c: [
//             {
//               d: [1, 2, 3]
//             },
//             {
//               d: [1, 2, 3]
//             },
//             {
//               d: [1, 2, 3]
//             }
//           ]
//         }
//       }
//     ]
//   }
// };

// // let res = omit("root.a.0.b.c.0.d", o);
// let res = omit("root.a..b.c", o);
// console.log(JSON.stringify(res, null, 2));

// let o = { a: 'top', b: [] }
// let result = omit('b..a', o)
// console.log('result', result)
