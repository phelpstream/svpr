const { curryN, trim, split, last } = require('ramda')

const path = curryN(2, function path(paths, obj) {
  var val = obj;
  var idx = 0;
  while (idx < paths.length) {
    if (val == null) return;
    let currentKey = trim(paths[idx])
    // console.log("currentKey", currentKey);
    if (Array.isArray(val) && currentKey.startsWith('{') && currentKey.endsWith('}')) {
      currentKey = currentKey.slice(1, -1)
      let separatorIndex = currentKey.indexOf('=')
      if (separatorIndex > -1) {
        let [key, matchValue] = [currentKey.substring(0, separatorIndex), currentKey.substring(separatorIndex + 1)]
        val = val.find(v => {
          let pathValue = path(split('.')(key), v)
          return pathValue === matchValue || (!isNaN(matchValue) ? pathValue === +matchValue : false)
        })
      } else if (currentKey.startsWith('$')) {
        if (currentKey === "$last") {
          val = last(val)
        }
      }

    } else {
      val = val[paths[idx]];
    }
    idx += 1;
  }
  return val;
});

module.exports = path


// let p = path(["persons", "{details.age=23}", "claims", "{id=123}"], { persons: [{ name: "Gabin", details: { age: 23 }, claims: [{ id: 123, ok: true }] }, { name: "Julie" }] })
// console.log("p", p);