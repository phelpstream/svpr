//@ts-check
const { is, curryN } = require('ramda')
const digBaseP = require('./digBaseP')
const Stop = require('./stop')

const digStopP = async (fn, object, { key, path } = {}) =>
  Promise.resolve(object).then(objectP => {
    if (Stop().isStop(objectP)) {
      return objectP.Then()
    } else {
      return digBaseP(digStopP, fn, objectP, { key, path })
    }
  })

module.exports = digStopP

// let o = {
//   title: 'title',
//   h2: 'h2',
//   arr: [1, 2],
//   detail: {
//     $s: 'ul li.item',
//     $a: 'text cmp'
//   },
//   re: {
//     cur: {
//       sive: 'ok'
//     }
//   }
// }

// ;(async () => {
//   let r = await digStopP((value, { key, path, at_filter_time, at_edit_time }) => {
//     // if (at_filter_time || at_edit_time) {
//       console.log('editing', 'key', key, 'value', value, 'path', path)
//     // }
//     return Stop(value)
//   }, o)

//   console.log(JSON.stringify(r, null, 2))
// })()
