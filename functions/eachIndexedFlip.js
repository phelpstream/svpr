const { flip } = require("ramda")
const eachIndexed = require("./eachIndexed")
module.exports = flip(eachIndexed)
