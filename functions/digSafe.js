//@ts-check
const { when, ifElse, is, mapObjIndexed, map, curryN, converge, identity } = require("ramda")

const digSafe = curryN(2, (fn, object) =>
  ifElse(is(Array), map(identity), when(is(Object), mapObjIndexed(converge(digSafe(fn), [identity, fn]))))(object)
)

module.exports = digSafe

// let temp = {
//   frame: {
//     type: Object,
//     properties: {
//       data: {
//         alt: ["d"],
//         type: Object
//       },
//       selector: {
//         alt: ["s"],
//         type: String
//       },
//       group: {
//         alt: ["g"],
//         type: Object,
//         additionalProperties: false
//       }
//     }
//   }
// }

// console.log(JSON.stringify(digSafe((val, index) => console.log(index), temp), null, 2))
