const { is } = require("ramda")

const ascend = (a, b) => {
  try {
    if (is(Object, a) && !is(Array, a)) return JSON.stringify(a).localeCompare(JSON.stringify(b))
  } catch (error) {
    // Might not be safe if circular, etc
  }
  if (is(String, a)) return a.localeCompare(b)
  return a - b
}

module.exports = ascend
