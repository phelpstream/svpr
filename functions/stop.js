//@ts-check

const { ifElse, when } = require('ramda')
const has = require('./has')

function Stop(value) {
  if (!(this instanceof Stop)) return new Stop(value)
  let data = value
  this.constructor = Stop
  this.Then = stopInstance => {
    return stopInstance ? stopInstance.Then() : data
  }
  this.isStop = possibleStop => {
    // console.log('possibleStop', possibleStop)
    let isStopped = has('constructor', possibleStop) && possibleStop.constructor.name === this.constructor.name
    // console.log('isStopped', isStopped)
    return isStopped
  }
  this.ifStopThen = input => when(Stop().isStop, Stop().Then)(input)
  this.ifStopThenElse = orElse => {
    // let isStop = Stop().isStop(orElse)
    // console.log("isStop", isStop, orElse);
    // if (isStop) {
    //   return Stop().Then
    // } else {
    //   return orElse
    // }
    return ifElse(Stop().isStop, Stop().Then, orElse)
  }
  return this
}

module.exports = Stop

// console.dir(Stop().Value(Stop("ok")))
// console.dir(Stop().isStop("ok"))
