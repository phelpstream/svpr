const R = require('ramda')
const set = require('./set')
const get = require('./get')
const has = require('./has')
const clone = require('./clone')
const pathways = require('./pathways')

const pick = R.curryN(2, (path, referenceObj, baseObj = {}) => {
  let pw = pathways(path, referenceObj)
  // console.log("pw", pw);
  return pw.reduce((pickedObj, p) => {
    let pathExists = has(p, referenceObj)
    if (pathExists) {
      pickedObj = set(p, clone(get(p, referenceObj)), pickedObj)
    }
    return pickedObj
  }, clone(baseObj))
})

module.exports = pick

// const o = {
//   root: {
//     a: [
//       {
//         b: {
//           c: [
//             {
//               d: [1, 2, 3]
//             }
//           ]
//         }
//       },
//       {
//         b: {
//           c: [
//             {
//               d: [1, 2, 3]
//             },
//             {
//               d: [1, 2, 3]
//             },
//             {
//               d: [1, 2, 3]
//             }
//           ]
//         }
//       }
//     ],
//     a2: true
//   }
// };

// let res = pick("root.a2", o);
// console.log(JSON.stringify(res, null, 2));
