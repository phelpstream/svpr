//@ts-check
const { __, is, map, isNil, reject, join, pipe, when, curryN, not, converge, identity } = require("ramda")
const get = require("./get")
const getOr = require("./getOr")
const getFlip = require("./getFlip")
const digStop = require("./digStop")
const Stop = require("./stop")

const parseStringWhenNumber = curryN(2, (path, obj) => {
  let parseToNumber = false
  if (path.startsWith("+")) {
    parseToNumber = true
    path = path.slice(1)
  }
  return parseToNumber ? +get(path, obj) : get(path, obj) 
})

const morph = curryN(2, (template, obj) => {
  return digStop(path => {
    if (is(String, path)) {
      let value = parseStringWhenNumber(path, obj)
      if (not(isNil(value))) return Stop(value)
    } else if (is(Array, path)) {
      let values = reject(isNil)(map(parseStringWhenNumber(__, obj), path)).join(" ")
      if (values) return Stop(values)
    } else {
      return path
    }
  }, template)
})

module.exports = morph

// let io = {
//   id: 1,
//   name: "LISCA SEVNICA",
//   mall_name: "LISCA SEVNICA [1]",
//   lat: "46.008437",
//   lng: "15.304351",
//   category: "Lisca, all",
//   address: "Prešernova 4",
//   address2: " ",
//   city: "Sevnica",
//   state: "",
//   postal: "8290",
//   country: "SI",
//   phone: "+386 7 81 64 320",
//   email: "trgovina.sevnica@lisca.si",
//   web: "",
//   hours1: "M: 8:00 - 19:00",
//   hours2: "T: 8:00 - 19:00",
//   hours3: "W: 8:00 - 19:00",
//   hours4: "T: 8:00 - 19:00",
//   hours5: "F: 8:00 - 19:00",
//   hours6: "S: 8:00 - 13:00",
//   hours7: "",
//   hours8: "",
//   featured: "true",
//   features: "Cheek"
// }

// console.log(
//   // JSON.stringify(
//   morph(
//     {
//       _uid: ["id"],
//       name: "name",
//       id: "id",
//       website: "web",
//       contactDetails: {
//         street: "address",
//         secondLineStreet: "address2",
//         city: "city",
//         postalCode: "postal",
//         country: "country",
//         state: "state",
//         latitude: "+lat",
//         longitude: "+lng",
//         phone: "phone",
//         email: "email"
//       },
//       category: "category"
//     },
//     io
//     // null
//   )
//   // ,
//   //   null,
//   //   2
//   // )
// )
