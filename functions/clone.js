//@ts-check
const { is } = require('ramda')
// const cloneObj = require('./cloneObj')

const cloneFn = require('fast-copy')

// const cloneFn = value => {
//   if (is(Object, value)) return cloneObj(value)
//   return clone(value)
// }

// found here: https://gist.github.com/c7x43t/38afee87bb7391efb9ac27a3c282e5ed
// function cloneFn(a) {
//   if (('object' != typeof a || null === a) && !(a instanceof Function)) return a
//   let b, d
//   const f = a.constructor
//   if (a[Symbol.iterator] instanceof Function) {
//     const g = a.length
//     switch (((b = new f(g)), f)) {
//       case Set:
//         for (let h of a) b.add(cloneFn(h))
//         break
//       case Map:
//         for (let [h, j] of a) b.set(h, cloneFn(j))
//     }
//     for (let h of Object.keys(a)) b[h] = cloneFn(a[h])
//   } else if (((d = Object.getOwnPropertyNames(a)), f !== Object)) {
//     switch (f) {
//       case Function:
//         let g = a.toString()
//         b = null === / \[native code\] /.exec(g) ? new f(/^.*?{(.*)}/.exec(g)[1]) : a
//         break
//       case RegExp:
//         b = new f(a.valueOf())
//         break
//       case Date:
//         b = new f(a)
//         break
//       case ArrayBuffer:
//         b = new f(new Int8Array(a).length)
//         break
//       default:
//         b = a
//     }
//     for (let g of d) b.hasOwnProperty(g) || (b[g] = cloneFn(a[g]))
//   } else {
//     b = {}
//     for (let g of d) b[g] = cloneFn(a[g])
//   }
//   for (let g of Object.getOwnPropertySymbols(a)) b[g] = cloneFn(a[g])
//   return b
// }

module.exports = cloneFn
