const { pipe, values, length } = require("ramda")
const size = pipe(values, length)
module.exports = size
// usage:
// size({ a: 1, b: 2, c: 3 }); // -> 3
// size(['a','b','c']); // -> 3
