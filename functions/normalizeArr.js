const { sort, identity, map } = require("ramda")
const ascend = require("./ascend")

const normalizeArr = (list, normalizeFn = identity) => {
  return sort(ascend, map(normalizeFn, list))
}

module.exports = normalizeArr

// console.log(normalizeArr([1, 5, 3, 0, 29, 12, 4]))
// console.log(normalizeArr(["g", "t", "h", "f", "i"]))
