const { curryN, flip, some } = require("ramda");
const has = require("./has");

const hasAny = curryN(2, function hasPath(keyPathList, obj) {
  // return some(flip(has)(obj), keyPathList);
});

module.exports = hasAny;

// console.log(hasAny(["something.ok", "something,notok"], { something: { ok: true } }));
