const pathArray = path => path.split(".").map(i => (isNaN(parseInt(i)) ? i : parseInt(i)));

module.exports = pathArray;
