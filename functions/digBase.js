//@ts-check
const { ifElse, pipe, is, mapObjIndexed, curryN, map, unary } = require("ramda")

const mapper = (digger, fn) => pipe(fn, digger(fn))
const digBase = curryN(3, (digger, fn, object) =>
  ifElse(is(Array), map(mapper(digger, fn)), ifElse(is(Object), mapObjIndexed(mapper(digger, fn)), unary(fn)))(object)
)

module.exports = digBase
