const { curryN } = require("ramda")
const digBase = require("./digBase")
const Stop = require("./stop")

const digStop = curryN(2, (fn, object) => Stop().ifStopThenElse(digBase(digStop, fn))(object))
module.exports = digStop

// exemple
// const { is } = require("ramda")

// let o = {
//   title: "title",
//   h2: "h2",
//   arr: [1, 2],
//   detail: {
//     $s: "ul li.item",
//     $a: "text cmp"
//   },
//   re: {
//     cur: {
//       sive: "ok"
//     }
//   }
// }

// console.log(
//   JSON.stringify(
//     digStop((value, index) => {
//       if (is(Object, value)) {
//         return value
//       } else if (is(String, value)) {
//         return Stop(
//           value
//             .split("")
//             .reverse()
//             .join("")
//         )
//       }
//       return value
//     }, o),
//     null,
//     2
//   )
// )
