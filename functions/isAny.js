const { pipe, not, isEmpty } = require("ramda")
const isAny = pipe(isEmpty, not)
module.exports = isAny

// console.log(isAny([]));
// console.log(isAny(["1"]));
