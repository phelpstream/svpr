//@ts-check
const { curryN } = require("ramda")
const captureBase = require("./captureBase")

const capture = curryN(2, (regex, value) => captureBase(false, null, regex, value))
module.exports = capture

// exemple

// let s = "Some boring text"
// let r = /(\w+)/gim

// let result = capture(r, s)
// console.dir(result)
