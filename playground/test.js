const { crunch } = require('./../index')

// console.log(length("Super cool"));

// let a = [10, 20, 30]
// console.log(a["0"]);
// console.log(a[0]);

let o = {
  title: 'title',
  h2: 'h2',
  arr: [1, 2],
  detail: {
    $s: 'ul li.item',
    $a: 'text cmp'
  },
  re: {
    cur: {
      sive: 'ok'
    }
  }
}

console.log(crunch(o, { keepLists: true }))
