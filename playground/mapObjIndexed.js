const { when, either, is, mapObjIndexed, curryN, reject, isNil, converge, unary, identity, map } = require("ramda")

// mapObjIndexed(converge(unary(dig), [identity, fn]))

let o = {
  one: "1",
  two: {
    ok: true,
    no: false
  },
  three: [
    {
      ok: true,
      no: false
    },
    {
      ok: true,
      no: false
    }
	]
	
ifElse(either(is(Array), is(Object)), mapObjIndexed(converge(unary(dig), [identity, fn])))
// mapObjIndexed(console.log, o)
// map(console.log, o)

const removeNulls = R.when(
	R.either(R.is(Array), R.is(Object)),
	R.pipe(
			R.reject(R.isNil),
			R.map(a => removeNulls(a))
	)
)