module.exports = {
  remove: require('./functions/remove'),
  path: require('./functions/path'),
  clone: require('./functions/clone'),
  next: require('./functions/next'),
  previous: require('./functions/previous'),
  symbols: require('./functions/symbols'),
  entries: require('./functions/entries'), // alias
  dig: require('./functions/dig'),
  digP: require('./functions/digP'),
  digStopP: require('./functions/digStopP'),
  digFlip: require('./functions/digFlip'),
  digStop: require('./functions/digStop'),
  // digStopFlip: require("./functions/digStopFlip"),
  digSafe: require('./functions/digSafe'),
  // digSafeFlip: require("./functions/digSafeFlip"),
  each: require('./functions/each'), // alias
  eachFlip: require('./functions/eachFlip'),
  eachIndexed: require('./functions/eachIndexed'),
  eachIndexedFlip: require('./functions/eachIndexedFlip'),
  every: require('./functions/every'), // alias
  rejectDeep: require('./functions/rejectDeep'),
  rejectDeepEmpty: require('./functions/rejectDeepEmpty'),
  timesFlip: require('./functions/timesFlip'),
  size: require('./functions/size'),
  ascend: require('./functions/ascend'),
  descend: require('./functions/descend'),
  normalizeArr: require('./functions/normalizeArr'),
  normalizeObj: require('./functions/normalizeObj'),
  normalize: require('./functions/normalize'),

  // Tests
  isAny: require('./functions/isAny'),

  // Parsing
  capture: require('./functions/capture'),
  captureAll: require('./functions/captureAll'),
  captureAs: require('./functions/captureAs'),
  captureAllAs: require('./functions/captureAllAs'),

  // Object
  cloneObj: require('./functions/cloneObj'),
  equals: require('./functions/equals'),
  crunch: require('./functions/crunch'),
  pathways: require('./functions/pathways'),
  omit: require('./functions/omit'),
  omitList: require('./functions/omitList'),
  pick: require('./functions/pick'),
  pickList: require('./functions/pickList'),
  set: require('./functions/set'),
  get: require('./functions/get'),
  getFlip: require('./functions/getFlip'),
  getOr: require('./functions/getOr'),
  getFlipOr: require('./functions/getFlipOr'),
  has: require('./functions/has'),
  hasAll: require('./functions/hasAll'),
  morph: require('./functions/morph'),

  // String
  stringify: require('./functions/stringify'),
  upper: require('./functions/upper'), // alias
  lower: require('./functions/lower'), // alias
  compact: require('./functions/compact'),
  capitalize: require('./functions/capitalize'),

  // Context
  Stop: require('./functions/stop'),
  Stack: require('./functions/stack')
}
